import os
import email
import argparse
import pandas as pd
from urllib.parse import urlparse

class Extractor:
    def __init__(self, fp, output_dir, verbose=False):
        self.fp = fp
        self.output_dir = output_dir
        self.verbose = verbose
        self.fdir = '{}.html_files'.format(os.path.basename(os.path.splitext(self.fp.name)[0]))

    @classmethod
    def extract_files(cls, file, output_dir, verbose=False):
        with open(file, 'r') as fp:
            yield cls(fp, output_dir, verbose)

    @staticmethod
    def partname_or_filename(fp, url):
        return os.path.basename(urlparse(url).path) or '{}.html'.format(os.path.basename(os.path.splitext(fp.name)[0]))

    def extract(self):
        message = email.message_from_file(self.fp)

        for part in message.walk():
            content_type = part.get_content_type()
            content_location = part['Content-Location']
            name = self.partname_or_filename(self.fp, content_location)

            if content_type.startswith('multipart'):
                continue

            self._write(name, part.get_payload(decode=True))

    def _write(self, name, content):
        newpath = ''

        if name.endswith('html'):
            newpath = os.path.join(self.output_dir, name)
        else:
            if not os.path.exists(os.path.join(self.output_dir, self.fdir)):
                os.mkdir(os.path.join(self.output_dir, self.fdir))

            newpath = os.path.join(self.output_dir, self.fdir, name)

        with open(newpath, 'wb') as fp:
            fp.write(content)


def parse(file_name):
    for ex in Extractor.extract_files(file_name, '.', False):
        ex.extract()
    file_name = os.getcwd()+'/'+file_name.split('/')[-1].split('.')[0]+".html_files/Mappe1.htm"
    data = pd.DataFrame(pd.read_html(file_name)[0])
    data.columns = data.iloc[0]
    print(data)

if __name__ == '__main__':
    parse("export.MHTML")
    
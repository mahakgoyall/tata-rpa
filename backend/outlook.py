from urllib.parse import quote, urlencode
import base64
import json
import time
import requests

client_id = '78234ec2-ea91-4a6a-b3ad-9a1d350a9dbb'
client_secret = 'J1o[3F=?{_J(![xJdpq+UM@=*=@=?6;.:;Mzt823*%>&l=[}?($-{&+9AM'

authority = 'https://login.microsoftonline.com'
authorize_url = '{0}{1}'.format(authority, '/common/oauth2/v2.0/authorize?{0}')


token_url = '{0}{1}'.format(authority, '/common/oauth2/v2.0/token')

scopes = ['openid',
          'User.Read',
          'Mail.Read']


def get_signin_url(redirect_uri):
    # Build the query parameters for the signin url
    params = {'client_id': client_id,
              'redirect_uri': redirect_uri,
              'response_type': 'code',
              'scope': ' '.join(str(i) for i in scopes)
              }

    signin_url = authorize_url.format(urlencode(params))

    return signin_url


# print(get_signin_url('http://localhost'))


# http://localhost/?code=Mcd71e5b5-4b70-708e-3182-41ba968a515a

def get_token_from_code(auth_code, redirect_uri):
    # Build the post form for the token request
    post_data = {'grant_type': 'authorization_code',
                 'code': auth_code,
                 'redirect_uri': redirect_uri,
                 'scope': ' '.join(str(i) for i in scopes),
                 'client_id': client_id,
                 'client_secret': client_secret
                 }

    r = requests.post(token_url, data=post_data)

    try:
        return r.json()
    except:
        return 'Error retrieving token: {0} - {1}'.format(r.status_code, r.text)


# auth_code = 'Mcd71e5b5-4b70-708e-3182-41ba968a515a'
# auth_code = 'OAQABAAIAAADCoMpjJXrxTq9VG9te-7FX_6JtESUJx_JIP6jN4r84nMzx5ndNzLv1QBJIqLn-RFIZZPkT_srzDvvwriGZaDy2rHYlazN3FCM_wG6XzUZxnu8GyXSRDhZ_jj4bhfX8mVs9LhfwlQ8-0FU_KcHS8Ua4MlXk1aONpe0n2sEz9ti96fmoAuTwHyXij_k07m2DX-1cjMzCPHvDx9oTe8MU9vx3n5y4bhxLdZQSPs81SEjz-sQrXTZs_RW5eL_8r5TwfzViTdk4B8P9Jt1eCmHZMq8A2Fg4LyB1QhU8o_YFTwUZ4FbXO6aAyupEr0NQ9mphdXawqbtSCUdm-GnFQQTB4J8hq1FKDxxwKYLwLToaDVhFTPGGCKcafxMXa9hxjElOw0JV0zcPBiohDYChDPPuAeeTzUeqzYC9s7dULzopQaL1_MsRTPZQfVuRwxvuD_UiFUx_60Nt9arKtQ1ik2DsFIZRchYEc9UUsLh7pcgPR-4CeDKbaOAfy8c3ryYwYFN4WtoDWKTtKBRXURZiQ7Dn8vOt4Q43XViGZbdgWkIP-Q_noOWdfC2MSqNNHVOQQqo0iDwZXEjDdFpvagjJM_LGHdTVLgmRHbAachmYDFN77cw43rhPg2foJwOJNz4uppxRRmAgAA'
auth_code = 'OAQABAAIAAADCoMpjJXrxTq9VG9te-7FX4HwGuCxnUCz-JhJySq5JEvTwc4JvEV1UPAWegUaY9bQFgtvDf5Cm_xNeaDnuIY_saCZbOf2ngJ6bGyAhSm7OSS6Dzg3cKp5u3ZccGpU1DeHcMD7tiVpCkwvHhoSdHG07AI7AdKyDT86iunwVnIodUdTTbFA-qakJt4wKFZ6cle4gMybiw8z_QkYmayA3SAbJog2wvJpJ17lqJbKdRDlLRJL8B0wx7a1WpYL3MVlyVKaUffTocnUeVrn6HRreJKfgcScZgEDhFo4eD3_Eo-9kur8PEn-K1Lu-Aye-t1s3bjRfTz95BUKb1NYmX4CtGIjSTpj32hiZjKlIoVhSmGOMqBw8sMTfd9IgyCwnfm1sgQsJYxEfej7Pil1HGUYYfsXGh6jecUDTO2CaXex2ioOw9bY2cOS8mnmrjBA1aWUTlETZKelkqTF84ih12hSwV8MoYsK0NgLeBlL_PpUbyLkXE9wOVt_bYpUIfXZalDh215dOoaiQn3yp57cU19wML1h51q73fXkh0DyziyhpcUgNV7ZI1oe8j007DGQVHM4GlUUT8oSg3DC8OvaBUC4pVVUidbQ0RpUuhAuoe_tkQgXqxSAA'
# auth_code = 'OAQABAAIAAADCoMpjJXrxTq9VG9te-7FX8k4Ko2MFy7BIqzqQ-CImddPq4ivHIn9X1cihp8LU4kDXCq5RQPn4N_f_LAhiPP-yKCzSHEzhEJG1M8B1__vS1pSUSCXvdxUNWiuGaJC3DHZ_O8sIR5GO8l73uJzCNGkWY0OfxgsORAycwQgV8Zcd4m3dpdz7H6_TSi9wDOQdNTz9hq5wGonCYhYTEPuiCPeI4IEJ-Dpzzj_1tSGj3pJMYybSGZfX4jGSEgDZHPLk0dVKGtbVNDJnwXod4w98TvXehSYvIDiwYsax0dwqej2dMGdO70uaEqeMom0cnPPDbz3M65aQeiMVtyC7WQ0hn1k4sWNy_RcN4woWTscT0b5AnvDOpjE-ABJGN1pyjRTqila9ihAreUA2TDAniR0t3OjK-ZcxJdA52AgBHGXhLyNhSwYmBU3DdMCbl22gN00bcz5IoIgM71xTbK-5uAsOZnCzwbY56HyfHD0n4mR7Q0IBev5rU8A0o0riu3zw_f2jR5myexWjSvv4CesXx025CR-YKu11oFS-x__YDBMI0LhZXiWbDWmhUwe9t6TsEpG583E7w5xPXZu_ONpYoq4qS39ycWxLJF2HWmWuRM07IfW5QjpRe0ewgZk_-f3ovbWYpuAgAA'
print(get_token_from_code(auth_code,'http://localhost'))
# token_dic = {'token_type': 'Bearer', 'scope': 'openid User.Read Mail.Read', 'expires_in': 3600, 'ext_expires_in': 3600, 'access_token': 'EwB4A8l6BAAURSN/FHlDW5xN74t6GzbtsBBeBUYAAUhWnrqDi7WHizblCh1FOIMUKAdREFEZV1ZULmrIz6mBVRiMrBtvMyRK9WZCvqhSU4VVm+fI72yoJto23xHw7gttHKxUltoqLRF1mDMDHGHKr8PQW2lJ/s4Y+0AFORNdk/LR113WtTxAyBDw8jPdf1GiWW1OaYSRmfv21/+C6wfjOq0jfoBmGpPnfPlAd2MIu5GevGPA5jTnMhX5Tk1F3pzB1iZqRVwTfcoPwtquJPlF/zMfB64JsyAZ+aaj/SHejTdsUtmHVgBeNlqEOipzOp0vBwwJJQ3ldH47Oo5TUD/VoDpdiBJZ0Zhl5EanC7lWtcRWkOVf/5an9rrn22FJ4kUDZgAACFi4cT8chtxLSALpZ3fGOLuOJn5xRrITqK+GFGc4pLUBRwn3KQ1VAbpHyOsER5MicK9KAs9fDdpHtficb1Fw0YAzfnMB1KWS73jeI1Dqr7rCeeGzAeYUtBAShuiEhzx1i0nB/esee5wckQ7JNO8K7SEtGTy1gqkgpFeZ8ThcIMZtbKf15cf5RAT5O6xSHUkd+VUV2m+WLSlclIVeh4lCZG4m1pIZUcjmyejtWGbQ5TJRcHtkBJqVUA0p7jgpkZ8Rf5iFQwnSeJOEqf7vaxztJbAV35/9i3Z0zVA8Im5nC23oK9r3ftjaasTZZ1ltcWpDDN4KdR++nnqJrl4mm+xU0gL9JL75vd+bAwKEW08kOB6oRTPKwkXLOtiNZZfSj2OA9blDRDXSBsocFXWfWyzPWBFXvCByFTxu9ycC4Eyd9mKLEvMMWXQsPDfT5Qmsu6szJQz6vo7OfKdzqEWcR35IYDsjIG2HekZj1ITJqPDiF8Sfwq7+xinckPEluV2kMJP6EKYhf44bI16KIBbCi7MC1nnscQbtQ5OnibpZOKl5uHU5KVSRw7H4t4tY/9lPzSalxrnBl8xCZ4Qe7pOZgDc979ZRk4Pa456qSdkc1dvTGIaW3Q5qx7RVgu55/Pi1rkWxlHrFHez5e+JrkwCrQTnUad4XY2co689IZZYd1prTkAfqnWs+9eVPlBZ0Wpw2nWMlyn125diOGNaOqMReSqPHIkAf/ch+6ULCQCOOJ2KO+jNuIjumWOrqW0eOyC2Ru68+OqYzDmNvy0+HPjRTfqZK35EvTIQC', 'id_token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6IjFMVE16YWtpaGlSbGFfOHoyQkVKVlhlV01xbyJ9.eyJ2ZXIiOiIyLjAiLCJpc3MiOiJodHRwczovL2xvZ2luLm1pY3Jvc29mdG9ubGluZS5jb20vOTE4ODA0MGQtNmM2Ny00YzViLWIxMTItMzZhMzA0YjY2ZGFkL3YyLjAiLCJzdWIiOiJBQUFBQUFBQUFBQUFBQUFBQUFBQUFFLXo3cW1OWlNMNHI2Zm1jQWFTazZrIiwiYXVkIjoiOWQ3YmEzMGEtYWQ0Ni00OGNjLTkzZjQtMjM1NGIxMjU5NGEzIiwiZXhwIjoxNTU2MTc1MjE2LCJpYXQiOjE1NTYwODg1MTYsIm5iZiI6MTU1NjA4ODUxNiwidGlkIjoiOTE4ODA0MGQtNmM2Ny00YzViLWIxMTItMzZhMzA0YjY2ZGFkIiwiYWlvIjoiRFdZc2R0WDghN0pYSTJmclUxMjM5emloVFVrMDV5Rk5xQ0ZubFZ6RkdDbmdMTSFjVzNWVlE1c0YwWmc3MVIyIXVXMGlkMmZwcU44bGxpKjNZeFJMM1lDakFUbThLek15allEQ3lqRDZNemNNbW1aU1lHaGNvV2RHT2dhZXFtQ2liSXpVKjA1R3djSW1kTDhJQUVncmllNCQifQ.qU1nz_d3lIUpLXzadddXDexz6DGHILM-Mgp2AaJBlk1poDHn7aqLk689m3nHAionWaghca04UFKkpU0Plbr_be0ubs8N28lj6mrSc7HJyuKaerL8XsC3TuSNVu6JTSbyDVECvBU7_Oe2k7QURbr9o-nc7Qvww2HPoLIfwVcVnb5bh69ItcMx1E3x6hxLQTKU012JDpy5RCUrMSWgVm3vkFqMYH2-0-9huqNpdGUTYLXxxnqEPVKfPMHvSmHLkobeQV_frkFaJzMDxEupYyCDC2Shz_7Z_Vbo6__mv6GFHbojC__xLIfiUNpSS8-Lg5-PUUJCd_Ft36MhPHR3o7MZ3g'}

# token_dic = {'token_type': 'Bearer', 'scope': 'Mail.Read openid User.Read profile email', 'expires_in': 3599, 'ext_expires_in': 3599, 'access_token': 'eyJ0eXAiOiJKV1QiLCJub25jZSI6IkFRQUJBQUFBQUFEQ29NcGpKWHJ4VHE5Vkc5dGUtN0ZYdjZyVkRRVTUwNHJPb3BIWVo1Q2lfSFEwMFRFVU1adHNNRFBGbzdjb09IVHJiMkJTZlhxZmRTSDU2QlBKN21IaUJLXzcxZHpWQmtxM3RtMzBEekg2R3lBQSIsImFsZyI6IlJTMjU2IiwieDV0IjoiSEJ4bDltQWU2Z3hhdkNrY29PVTJUSHNETmEwIiwia2lkIjoiSEJ4bDltQWU2Z3hhdkNrY29PVTJUSHNETmEwIn0.eyJhdWQiOiJodHRwczovL2dyYXBoLm1pY3Jvc29mdC5jb20iLCJpc3MiOiJodHRwczovL3N0cy53aW5kb3dzLm5ldC9lNGUzNDAzOC1lYTFmLTQ4ODItYjZlOC1jY2Q3NzY0NTljYTAvIiwiaWF0IjoxNTU2MDk0NzgwLCJuYmYiOjE1NTYwOTQ3ODAsImV4cCI6MTU1NjA5ODY4MCwiYWNjdCI6MCwiYWNyIjoiMSIsImFpbyI6IkFTUUEyLzhMQUFBQXVYcHZEc2c3ZjhEUC8rb2I4TXBHdUtIeElFYUF6bHFJNmxHZEdkRmRQbTA9IiwiYW1yIjpbInB3ZCJdLCJhcHBfZGlzcGxheW5hbWUiOiJPdXRsb29rIEFQSSBJbXRlZ3JhdGlvbiB3aXRoIFB5dGhvbiIsImFwcGlkIjoiNzgyMzRlYzItZWE5MS00YTZhLWIzYWQtOWExZDM1MGE5ZGJiIiwiYXBwaWRhY3IiOiIxIiwiZmFtaWx5X25hbWUiOiJDZWxlYmFsIiwiZ2l2ZW5fbmFtZSI6IlRlc3RpbmciLCJpcGFkZHIiOiIxMDMuNjYuNzMuMTEzIiwibmFtZSI6IlRlc3RpbmcgQ2VsZWJhbCIsIm9pZCI6IjU2OTBkOTAzLWIyOTItNDExNS1iOGE3LTJkZDlmYzE3NzM3YSIsInBsYXRmIjoiMTQiLCJwdWlkIjoiMTAwMzIwMDAzNkEyNDgxQiIsInNjcCI6Ik1haWwuUmVhZCBvcGVuaWQgVXNlci5SZWFkIHByb2ZpbGUgZW1haWwiLCJzdWIiOiJGMmZzVGpSeUhvRFVsOW9Zd3dJVlZrOTgyOGp6ZzZLbWFXV1p3S3pPSnVrIiwidGlkIjoiZTRlMzQwMzgtZWExZi00ODgyLWI2ZTgtY2NkNzc2NDU5Y2EwIiwidW5pcXVlX25hbWUiOiJ0ZXN0aW5nLmNlbGViYWxAY2VsZWJhbHRlY2guY29tIiwidXBuIjoidGVzdGluZy5jZWxlYmFsQGNlbGViYWx0ZWNoLmNvbSIsInV0aSI6Ijg3YXV5TlZpZUVtc0NiRFFuR1FhQUEiLCJ2ZXIiOiIxLjAiLCJ4bXNfc3QiOnsic3ViIjoiRGJvdFJfeVlJOTNGOXJwWGV1YzQxa0RrdmV3SkFlRnJJM0Z4cV9ZY01MMCJ9LCJ4bXNfdGNkdCI6MTQ4MzE3ODg2MH0.MbfrZSZgvw51hKCIh6151oGIKEK1DpYb2jnSGuXP2QN1B6F7ojmUa5Yes_PL96Czd1q5w-Q18OPIt5FwkR4__DaXZlTrDASdIVsAMPQupdDiZjRx2QzQC4NgrXPkkDU15ppy_MNJFMz4aDzbh4xHn8RmTK08dqqAy5Qz7sPgeFbIhjrQlYLzQb_NtxEk1kra0pL7D5LQhN4hRT8LPlYfszR4iTT55Qrp-U5f6-aY5P9RQwJnqOkb_sJgmkxcneQ1vKECM3hhUoLyhvNkt9SztZZs3oDTQZlp7Ha157xY-akHb1tI1s9MNTz1e8cqIwpELS12ey9IptLhpLui5tnlQA', 'id_token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6IkhCeGw5bUFlNmd4YXZDa2NvT1UyVEhzRE5hMCJ9.eyJhdWQiOiI3ODIzNGVjMi1lYTkxLTRhNmEtYjNhZC05YTFkMzUwYTlkYmIiLCJpc3MiOiJodHRwczovL2xvZ2luLm1pY3Jvc29mdG9ubGluZS5jb20vZTRlMzQwMzgtZWExZi00ODgyLWI2ZTgtY2NkNzc2NDU5Y2EwL3YyLjAiLCJpYXQiOjE1NTYwOTQ3ODAsIm5iZiI6MTU1NjA5NDc4MCwiZXhwIjoxNTU2MDk4NjgwLCJhaW8iOiJBVFFBeS84TEFBQUFNRGhOa2JYR2pWczhxUWpud3IzOWRxOVB6N3NhbzM2UmdkSlJham11WGs5ekJwWjFHcUVxTGJaL29KaE1lVGVRIiwic3ViIjoiRGJvdFJfeVlJOTNGOXJwWGV1YzQxa0RrdmV3SkFlRnJJM0Z4cV9ZY01MMCIsInRpZCI6ImU0ZTM0MDM4LWVhMWYtNDg4Mi1iNmU4LWNjZDc3NjQ1OWNhMCIsInV0aSI6Ijg3YXV5TlZpZUVtc0NiRFFuR1FhQUEiLCJ2ZXIiOiIyLjAifQ.hZZrXNOWHzrTW7UCDxPMaR3uoE4qFNZ5wsD7ZQ8R6SS9FVP8mACEX4lg3B-GmS8zrpf149GTUaHbFKspPF_mxetEPhwVk25fiGVf6IVKOZ9wvek63e4gboiiM67-C9eq0wHDRikPpbfavCStxReDzuUhweoZrPsXyKBBoumpZ6FriWaxRZfwgdWGRZsr-NZvjj6DXN1JFPMaeJKhTbHaneySnplq_37-OjBZi9mysyEAjpxSEE6gd0dtCcdhRscPyuZfQUTSWadcHR0vZXXIuGqkrPgtOIFtG8srTieTnX2Q1QIKy88RZ4I9M7wJfWWp7JnpCz3QTcLVeYj92bVQjg'}
token_dic = {'token_type': 'Bearer', 'scope': 'Mail.Read openid User.Read profile email', 'expires_in': 3600, 'ext_expires_in': 3600, 'access_token': 'eyJ0eXAiOiJKV1QiLCJub25jZSI6IkFRQUJBQUFBQUFEQ29NcGpKWHJ4VHE5Vkc5dGUtN0ZYYUZBclRmczRrS2k4VGFqRkRwNjlyWFk0QzJQYWNPdzhMQWR6LWJVaTY5NTJHc3I3Zmp3Y084U0hNZE82aVZEeEttaXpaRzlfNnNTQnYzRHhnNl9RLVNBQSIsImFsZyI6IlJTMjU2IiwieDV0IjoiSEJ4bDltQWU2Z3hhdkNrY29PVTJUSHNETmEwIiwia2lkIjoiSEJ4bDltQWU2Z3hhdkNrY29PVTJUSHNETmEwIn0.eyJhdWQiOiJodHRwczovL2dyYXBoLm1pY3Jvc29mdC5jb20iLCJpc3MiOiJodHRwczovL3N0cy53aW5kb3dzLm5ldC9lNGUzNDAzOC1lYTFmLTQ4ODItYjZlOC1jY2Q3NzY0NTljYTAvIiwiaWF0IjoxNTU2MDk2OTkyLCJuYmYiOjE1NTYwOTY5OTIsImV4cCI6MTU1NjEwMDg5MiwiYWNjdCI6MCwiYWNyIjoiMSIsImFpbyI6IjQyWmdZRmk5b20xcUpPdkZVK2RXQmlsZXUrMi9zZlBEdTFoTEViN1RmV2RpUFUxVDRzTUEiLCJhbXIiOlsicHdkIl0sImFwcF9kaXNwbGF5bmFtZSI6Ik91dGxvb2sgQVBJIEltdGVncmF0aW9uIHdpdGggUHl0aG9uIiwiYXBwaWQiOiI3ODIzNGVjMi1lYTkxLTRhNmEtYjNhZC05YTFkMzUwYTlkYmIiLCJhcHBpZGFjciI6IjEiLCJmYW1pbHlfbmFtZSI6IkNlbGViYWwiLCJnaXZlbl9uYW1lIjoiVGVzdGluZyIsImlwYWRkciI6IjEwMy42Ni43My4xMTMiLCJuYW1lIjoiVGVzdGluZyBDZWxlYmFsIiwib2lkIjoiNTY5MGQ5MDMtYjI5Mi00MTE1LWI4YTctMmRkOWZjMTc3MzdhIiwicGxhdGYiOiIxNCIsInB1aWQiOiIxMDAzMjAwMDM2QTI0ODFCIiwic2NwIjoiTWFpbC5SZWFkIG9wZW5pZCBVc2VyLlJlYWQgcHJvZmlsZSBlbWFpbCIsInN1YiI6IkYyZnNUalJ5SG9EVWw5b1l3d0lWVms5ODI4anpnNkttYVdXWndLek9KdWsiLCJ0aWQiOiJlNGUzNDAzOC1lYTFmLTQ4ODItYjZlOC1jY2Q3NzY0NTljYTAiLCJ1bmlxdWVfbmFtZSI6InRlc3RpbmcuY2VsZWJhbEBjZWxlYmFsdGVjaC5jb20iLCJ1cG4iOiJ0ZXN0aW5nLmNlbGViYWxAY2VsZWJhbHRlY2guY29tIiwidXRpIjoiSzh1VkdJdGJua1M4TVVPdnRnOVFBQSIsInZlciI6IjEuMCIsInhtc19zdCI6eyJzdWIiOiJEYm90Ul95WUk5M0Y5cnBYZXVjNDFrRGt2ZXdKQWVGckkzRnhxX1ljTUwwIn0sInhtc190Y2R0IjoxNDgzMTc4ODYwfQ.h_1x97yPuS4Q8iQ7MBbBhgRnF9Dn3veelp9H2bTRgyqKOQxcjh_2X9S-UCqvt_TikdArCf2LMJGujf8E5uPBuLsHpBJo3aBeTagOrR9fdnXaPFOd51ju2nbEnW60ii5GpesLh8ViTd4PeckxmL85eURV3LZKMgirz7EiwLujrWLB3IJkgk1JZNgBaWI4Q-b6VoIU2gnltpsAZ6P9x64QnCS7-BgF1mim_E3OyWUuTltIocgwm7ld8EYVl9m-NXmqb_-ZvsYc_-XSbuWPlgVqETQpyYTN4qyUO__4QccmSYFyYtbCMkV5JkqPDKv0J_Fu5N474dFQa-uZ1c5DPK0fEA', 'id_token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6IkhCeGw5bUFlNmd4YXZDa2NvT1UyVEhzRE5hMCJ9.eyJhdWQiOiI3ODIzNGVjMi1lYTkxLTRhNmEtYjNhZC05YTFkMzUwYTlkYmIiLCJpc3MiOiJodHRwczovL2xvZ2luLm1pY3Jvc29mdG9ubGluZS5jb20vZTRlMzQwMzgtZWExZi00ODgyLWI2ZTgtY2NkNzc2NDU5Y2EwL3YyLjAiLCJpYXQiOjE1NTYwOTY5OTIsIm5iZiI6MTU1NjA5Njk5MiwiZXhwIjoxNTU2MTAwODkyLCJhaW8iOiJBVFFBeS84TEFBQUFGamtHdUlGQzhOeThiQlRmVDZKV2x6RTUyWmZCaUxhMlVlTENGUm5wZmNJbkxFTFRJN21pYlpTU3BFMEJFS3Q3Iiwic3ViIjoiRGJvdFJfeVlJOTNGOXJwWGV1YzQxa0RrdmV3SkFlRnJJM0Z4cV9ZY01MMCIsInRpZCI6ImU0ZTM0MDM4LWVhMWYtNDg4Mi1iNmU4LWNjZDc3NjQ1OWNhMCIsInV0aSI6Iks4dVZHSXRibmtTOE1VT3Z0ZzlRQUEiLCJ2ZXIiOiIyLjAifQ.rbwI0kMVeTpaB4F9p_Cly69uSBtr2OKUlTyFiLEJqfKG8A6wsL6wzAsDAbbRvPFT1eGfLurA9si6YNUOp-OWIHmWTL7_7c3_MMWc-4jrwMxERU_wUZc1XjFoOTQey8cxTG4W3r1pEUPcEoeSCySNxFWWMsVM5rnWsQv6mqFFwZXASBlr2JC3_avKAlvUArZ8byOjojPmRUjLfWakTqNd1Lrrq5L7zzSco4NZ0wYNFXF9GJQ0owLVohBIY5iD09mKIrYzQEQZM8ElsoNG17t0bjcOrmAE33LmfYzzLzrIISryHPlle5PzcyUToPlKnYlJh2WdhGnRsoD155YJorH0mg'}
graph_endpoint = 'https://graph.microsoft.com/v1.0{0}'
import requests
import uuid
import json

graph_endpoint = 'https://graph.microsoft.com/v1.0{0}'

# Generic API Sending


def make_api_call(method, url, token, payload=None, parameters=None):
    # Send these headers with all API calls
    headers = {'User-Agent': 'python_tutorial/1.0',
               'Authorization': 'Bearer {0}'.format(token),
               'Accept': 'application/json'}

    # Use these headers to instrument calls. Makes it easier
    # to correlate requests and responses in case of problems
    # and is a recommended best practice.
    request_id = str(uuid.uuid4())
    instrumentation = {'client-request-id': request_id,
                       'return-client-request-id': 'true'}

    headers.update(instrumentation)

    response = None

    if (method.upper() == 'GET'):
        response = requests.get(url, headers=headers, params=parameters)
    elif (method.upper() == 'DELETE'):
        response = requests.delete(url, headers=headers, params=parameters)
    elif (method.upper() == 'PATCH'):
        headers.update({'Content-Type': 'application/json'})
        response = requests.patch(
            url, headers=headers, data=json.dumps(payload), params=parameters)
    elif (method.upper() == 'POST'):
        headers.update({'Content-Type': 'application/json'})
        response = requests.post(url, headers=headers,
                                 data=json.dumps(payload), params=parameters)

    return response



def get_me(access_token):
  get_me_url = graph_endpoint.format('/me')

  # Use OData query parameters to control the results
  #  - Only return the displayName and mail fields
  query_parameters = {'$select': 'displayName,mail'}

  r = make_api_call('GET', get_me_url, access_token, "", parameters = query_parameters)

  if (r.status_code == requests.codes.ok):
    return r.json()
  else:
    return "{0}: {1}".format(r.status_code, r.text)


# print(get_me(token_dic['access_token']))

# Get only unread messages
def get_my_messages(access_token):
  get_messages_url = graph_endpoint.format('/me/mailfolders/inbox/messages?$filter=isRead eq false')

  # Use OData query parameters to control the results
  #  - Only first 10 results returned
  #  - Only return the ReceivedDateTime, Subject, and From fields
  #  - Sort the results by the ReceivedDateTime field in descending order
  query_parameters = {'$top': '10',
                      '$select': 'receivedDateTime,subject,from',
                      '$orderby': 'receivedDateTime DESC'}

  r = make_api_call('GET', get_messages_url, access_token, parameters = query_parameters)

  if (r.status_code == requests.codes.ok):
    return r.json()
  else:
    return "{0}: {1}".format(r.status_code, r.text)


# print(get_my_messages(token_dic['access_token']))

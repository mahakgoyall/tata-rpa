# from O365 import Account

# client_id = '0e1bb6d0-b8c2-47d0-887c-c31203693c39'
# client_secret = 'MX-3YXU3@UYT0aA=wcA*M36c[oJ]H1M3'


# credential = (client_id, client_secret)
# account = Account(credential)

# if not account.is_authenticated:
#   account.authenticate(scopes=['basic', 'mailbox', 'message_all'])

# mailbox = account.mailbox()
# inbox = mailbox.inbox_folder()

# read = 0
# unread = 0
# for message in inbox.get_messages(limit=10000):
#   print(message.object_id)
#   read += 1
#   if not message.is_read:
#       unread += 1
#       msg_status, date = outlook.check_approval(message.get_body_text())
#         if msg_status:
#             print('Approved')
#         else:
#             print('Not Approved')
#       # print(message)
#   break


# print('Total: ',read)
# print('Unread:', unread)



from O365 import Account
import re

msg_obj_id = 'from_file'


class Outlook(object):

    def __init__(self):
        self.client_id = '0e1bb6d0-b8c2-47d0-887c-c31203693c39'
        self.client_secret = 'MX-3YXU3@UYT0aA=wcA*M36c[oJ]H1M3'
        self.credential = (self.client_id, self.client_secret)

    def login(self):
        self.account = Account(self.credential)

    def authenticate(self):
        self.account.authenticate(scopes=['basic', 'mailbox'])

    def get_inbox_messages(self, msg_limit=50):
        mailbox = self.account.mailbox()
        self.inbox = mailbox.inbox_folder()
        self.messages = self.inbox.get_messages(limit=200)

    def check_approval(self, body):
        check_date = re.search('\d+/\d+/\d+', body)
        date = ''
        try:
            date = body[check_date.start():check_date.end()]
        except Exception as e:
            print("Date is missing in the Email.")

        if 'not approved' in body.lower():
            flag = "not approved"
        else:
            flag = "Approved"

        return flag, date


outlook = Outlook()
outlook.login()

if not outlook.account.is_authenticated:
    outlook.authenticate()

outlook.get_inbox_messages(10)
f = open("mail_id_store.txt", "r")
contents = f.read()
f.close()

flag_write = True
for message in outlook.messages:
    if str(contents) == str(message.object_id):
        break
    if flag_write:
        f = open("mail_id_store.txt","w+")
        f.write(message.object_id)
        f.close()
        flag_write = False
    if not message.is_read:
        # print("message.mail_id: ", message.mail_id)
        msg_status, date = outlook.check_approval(message.get_body_text())
        print(msg_status, date)
        message.mark_as_read()
    

   

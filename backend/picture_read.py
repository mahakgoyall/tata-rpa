from PIL import ImageGrab, Image
import win32com.client as win32

excel = win32.gencache.EnsureDispatch('Excel.Application')
workbook = excel.Workbooks.Open("D:\\Mahak\\TataRPA\\backend\\app_101_.xlsx")

for sheet in workbook.Worksheets:
	for i, shape in enumerate(sheet.Shapes):
		if shape.Name.startswith('Picture'):
			print(shape.Name)
			shape.Copy()
			image = ImageGrab.grabclipboard()
			image.save('pictures\\' + str(shape.Name) + '.jpg')

			im = Image.open('pictures\\' + str(shape.Name) + '.jpg')
			# rgb_im = im.convert('RGB')
			# r, g, b = rgb_im.getpixel((1, 1))
			pix_val = list(im.getdata())
			r = [tup[0] for tup in pix_val]
			g = [tup[1] for tup in pix_val]
			if sum(r) > sum(g):
				print("Red it is.")
			else:
				print("Green it is.")
		
import re

body_contents =  ['Hello,', '\n', '\n ', '\n', '\nApproved.', '\n', '\n ', '\n', '\n15/04/2019', '\n', '\n ', '\n', '\nThanks', '\n', '\n ', '\n']
given_date = ""
for elem in body_contents:
	dates = re.search(r'(\d+/\d+/\d+)', elem)
	if dates:
		given_date = elem
		break
print("found date", str(given_date))
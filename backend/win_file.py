import sys
import configparser
import time
import sys
import csv
import os
from datetime import datetime
import win32com.client
import pandas as pd
import yagmail
import numpy as np
from urllib.parse import urlparse
import argparse
import email

# yagmail setup
yag = yagmail.SMTP('mahak.goyal@celebaltech.com', host='smtp.office365.com', 
	port=587, smtp_ssl=False, smtp_starttls=True)

source_doctype = {
	'KR' : 'Critical',
	'KG' : 'Data',
	'RE' : 'Exp Claim',
	'KC' : 'NO',
	'VT' : 'PO',
	'AB' : 'Tax',
	'BR' : 'Voice',
	'JV' : 'Critical',
	'KA' : 'Data',
	'RU' : 'Exp Claim',
	'SG' : 'NO',
	'UE' : 'PO',
	'SA' : 'Tax'
}

class Extractor:
    def __init__(self, fp, output_dir, verbose=False):
        self.fp = fp
        self.output_dir = output_dir
        self.verbose = verbose
        self.fdir = '{}.html_files'.format(os.path.basename(os.path.splitext(self.fp.name)[0]))

    @classmethod
    def extract_files(cls, file, output_dir, verbose=False):
        with open(file, 'r') as fp:
            yield cls(fp, output_dir, verbose)

    @staticmethod
    def partname_or_filename(fp, url):
        return os.path.basename(urlparse(url).path) or '{}.html'.format(os.path.basename(os.path.splitext(fp.name)[0]))

    def extract(self):
        message = email.message_from_file(self.fp)

        for part in message.walk():
            content_type = part.get_content_type()
            content_location = part['Content-Location']
            name = self.partname_or_filename(self.fp, content_location)

            if content_type.startswith('multipart'):
                continue

            self._write(name, part.get_payload(decode=True))

    def _write(self, name, content):
        newpath = ''

        if name.endswith('html'):
            newpath = os.path.join(self.output_dir, name)
        else:
            if not os.path.exists(os.path.join(self.output_dir, self.fdir)):
                os.mkdir(os.path.join(self.output_dir, self.fdir))

            newpath = os.path.join(self.output_dir, self.fdir, name)

        with open(newpath, 'wb') as fp:
            fp.write(content)


def CRR_Entity_List():
	crr_file_na_1 = pd.read_excel('CRR_2.xlsx', sheet_name = 'Sheet1')
	print("crr_file_na_1: ", crr_file_na_1)
	print(ss)
	crr_df_1 = crr_file_na_1[crr_file_na_1['Payment Block'].isnull()]
	crr_df_1 = crr_df_1.dropna(axis=0, subset=['Company Code'])
	crr_df_1['Company Code'] = crr_df_1['Company Code'].astype(int)

	payment_sheet = pd.read_excel('Payment lists.xlsx')

	# Create BATCHES for the given CRR and add as column in Dataframe
	unique_company_code = crr_df_1['Company Code'].unique().tolist()
	CRR_List = []
	for company_code in unique_company_code:
		# First letter of doc currency and Doc Type to be appended with company code to make batches
		batch_str = str(company_code)
		batch_str += (crr_df_1.loc[crr_df_1['Company Code'] == company_code, 'Document currency'].iloc[0])[:1]
		batch_str += (crr_df_1.loc[crr_df_1['Company Code'] == company_code, 'Document Type'].iloc[0])[:1]

		# Find Payment Method for value in Domestic Currency of an Entity
		payment_m = payment_sheet.loc[payment_sheet['Entity'] == company_code, ['Domestic Currency for Each Entity', 'Payment Method']]
		payment_m = payment_m[payment_m['Domestic Currency for Each Entity'] != '-']
		payment_str = payment_m['Payment Method'].values.tolist()
		print("payment_str: ", payment_str, " : ", company_code)
		if len(payment_str) == 0:
			payment_str = ''
		else:
			payment_str = payment_str[0].replace(",", "").replace(" ", "")

		# Create String for Vendor codes
		vendor_codes = crr_df_1.loc[crr_df_1['Company Code'] == company_code, 'Vendor']
		vendor_codes_list = vendor_codes.values.tolist()
		vendor_codes_list_unique = list(set(vendor_codes_list))
		vendor_codes_str = ''
		for code in vendor_codes_list_unique:
			vendor_codes_str += code + ", "
		vendor_codes_str = vendor_codes_str[:-2]

		# Unique currency string creation
		currency = crr_df_1.loc[crr_df_1['Company Code'] == company_code, 'Document currency']
		currency_list = currency.values.tolist()
		currency_list_unique = list(set(currency_list))
		currency_str = ''
		for curr in currency_list_unique:
			currency_str += curr + ", "
		currency_str = currency_str[:-2]
		CRR_List.append(list([str(company_code), batch_str, payment_str, vendor_codes_str, currency_str]))
	print("CRR_List: ", CRR_List)
 

def main_func():
	file_name = "export.MHTML"
	for ex in Extractor.extract_files(file_name, '.', False):
		ex.extract()
	file_name = os.getcwd()+'/'+file_name.split('/')[-1].split('.')[0]+".html_files/Mappe1.htm"
	crr_file_na_1 = pd.DataFrame(pd.read_html(file_name)[0])
	crr_file_na_1.columns = crr_file_na_1.iloc[0]

	# crr_file_na_1 = pd.read_excel('CRR_2.xlsx', sheet_name = 'Sheet1')
	reference_sheet = pd.read_excel('Entities with Bank Accounts & payment mode.xlsx')
	# 
	crr_df_1 = crr_file_na_1[crr_file_na_1['Payment Block'].isnull()]
	crr_df_1 = crr_df_1.dropna(axis=0, subset=['Company Code'])
	# crr_df_1 = crr_file_na[crr_file_na['Company Code'] != '']
	# print(crr_df_1[:5])
	crr_df_1['Company Code'] = crr_df_1['Company Code'].astype(int)
	# print("crr_df_1['Company Code']: ", crr_df_1['Company Code'])
	# crr_df_1 = crr_df_1[crr_df_1['Company Code'].str.lstrip('0')]

	ref_sheet = pd.DataFrame()
	ref_sheet['Entity'] = reference_sheet['Entity'].astype(int)
	ref_sheet['Document currency'] = reference_sheet['Crcy'].values
	ref_sheet['flag'] = 1

	# Logic for vendor source clubbed amount values
	# Create Source using the mapping with Doc type and club with Vendor
	crr_df_1['Source'] = crr_df_1['Document Type']
	crr_df_1.replace({"Source" : source_doctype}, inplace=True)
	vendor_source_df = crr_df_1.groupby(['Source','Vendor']).agg({'Amount in doc. curr.': 'sum'}).reset_index()
	# vendor_source_df = crr_df_1.groupby(['Source','Vendor']).agg({'Batches' :'unique', 'Amount in doc. curr.': 'sum'}).reset_index()
	# print("vendor_source_df: ", vendor_source_df)

	# Merge dataframes on Entity and Doc curr to find matches using flag column
	entity_curr_df = pd.merge(crr_df_1, ref_sheet, how='left', 
		left_on=['Company Code', 'Document currency'], 
		right_on=['Entity', 'Document currency'])
	entity_curr_df.to_csv("entity_curr_df.csv", index=False)
	# add total amount column corresponds to flag value
	entity_curr_df['f_amount'] = np.where(entity_curr_df['flag'] == 1, 
		entity_curr_df['Amount in doc. curr.'], entity_curr_df['Amount in local currency'])
	# add currency column corresponds to flag value(default curr is USD)
	entity_curr_df['curr_var'] = np.where(entity_curr_df['flag'] == 1, 
		entity_curr_df['Document currency'], entity_curr_df['Local Currency'])
	entity_curr_df.loc[entity_curr_df['flag'] != 1, 'Document currency'] = "USD"
	print("entity_curr_df without sum: ", entity_curr_df[:5])
	# sum the amount of new column added a/c to condition.
	entity_curr_df_1 = entity_curr_df.groupby(['Company Code', 'Document currency'])['f_amount'].sum().reset_index()

	# Add only negative amount to the final df
	entity_curr_df_negative = entity_curr_df_1[entity_curr_df_1['f_amount'].str.contains("-")]
	entity_curr_df_negative.to_csv("entity_currency_df.csv", index=False)
	print(entity_curr_df_negative[:10])
	# yag.send(to='ayush.garg@celebaltech.com',subject='Entity Currency File',
	# 	contents='find attachment', attachments="entity_currency_df.csv")


if __name__ == '__main__':
	CRR_Entity_List()